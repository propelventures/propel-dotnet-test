﻿using Dapper;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace business.framework
{
    public sealed class ProcessGST
    {
        private readonly string _databaseConnection;
        private readonly string _baseServiceEndpoint;

        protected string FromDate { get; private set; }
        protected string ToDate { get; private set; }

        public ProcessGST()
        {
            _databaseConnection = ConfigurationManager.AppSettings["DatabaseConnection"];
            _baseServiceEndpoint = ConfigurationManager.AppSettings["BaseServiceEndpoint"];
        }

        public async Task<bool> PrepareAndSendReport(string customerId, DateTime fromDateLocal, int numMonths)
        {
            try
            {
                FromDate = fromDateLocal.ToString("yyyy-MM-dd HH:mm:ss");
                ToDate = fromDateLocal.AddMonths(numMonths).ToString("yyyy-MM-dd HH:mm:ss");

                var connection = new SqlConnection(_databaseConnection);

                var data = await connection.QueryAsync(
                    $@"SELECT pr.ItemValue 
					FROM PurchaseRows pr 
                    JOIN Purchase p ON p.Id = pr.PurchaseId 
                    WHERE p.CustomerId = '{customerId}' 
                        AND p.PurchaseDateUTC >= '{FromDate}' 
                        AND p.PurchaseDateUTC < '{ToDate}'");

                var purchasesTotal = 0m;
                var purchasesGstTotal = 0m;

                foreach (var pdata in data)
                {
                    purchasesTotal += Math.Round(pdata.ItemValue * 1.1m, 2);
                    purchasesGstTotal += Math.Round(pdata.ItemValue * .1m, 2);
                }

                var data2 = await connection.QueryAsync(
                    $@"SELECT ir.ItemPrice 
					FROM InvoiceRows ir 
                    JOIN Invoice i ON i.Id = ir.InvoiceId 
                    WHERE i.CustomerId = '{customerId}' 
                        AND i.InvoiceDateUTC >= '{FromDate}' 
                        AND i.InvoiceDateUTC < '{ToDate}'");

                var invoicesTotal = 0m;
                var invoicesGstTotal = 0m;

                foreach (var idata in data2)
                {
                    invoicesTotal += Math.Round(idata.ItemPrice * 1.1m, 2);
                    invoicesGstTotal += Math.Round(idata.ItemPrice * .1m, 2);
                }

                var t = SendData(customerId, 
                    purchasesTotal, purchasesGstTotal, 
                    invoicesTotal, invoicesGstTotal);
                t.Wait();
            }
            catch
            {
                return false;
            }
            return true;
        }

        private async Task SendData(string customerID, decimal purchasesTotal, decimal purchasesGstTotal, 
            decimal invoicesTotal, decimal invoicesGstTotal)
        {
            using (var client = new HttpClient { BaseAddress = new Uri(_baseServiceEndpoint) })
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "0vr67gh38dj3djsh59dkp32");
                var response = await client.PostAsync("/api/receiveGstData", new StringContent($"{{ \"customerId\" : \"{customerID}\", \"purchases\" : {{ \"total\" : \"{purchasesTotal}\", \"gst\" : \"{purchasesGstTotal}\" }}, \"invoices\" : {{ \"total\" : \"{invoicesTotal}\", \"gst\" : \"{invoicesGstTotal}\" }} }}", Encoding.UTF8, "application/json"));
            }
        }
    }
}
